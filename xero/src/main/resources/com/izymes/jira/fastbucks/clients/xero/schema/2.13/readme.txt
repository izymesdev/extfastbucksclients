Schema files can be found here: https://github.com/XeroAPI/XeroAPI-Schemas/tree/master/v2.00

a tarball can be downloaded from here: https://github.com/XeroAPI/XeroAPI-Schemas/tarball/master

When updating the schemas, create a new directory for the current version, download the tarball, uncompress it
& copy the schemas to the folder then update the xero.api.version peroperty in the POM to point at the new folder

Next run an 'mvn clean jaxb:generate' to regenerate the /target/generated-sources,
then a quit test 'mvn package' - this will most likely fail with missing symbol errors,
when this happens open up the generated classes in /target/generated-sources and the generated files should
contain a 'catch-all' comment which states what it is about the schema that is incorrect,
often this takes the form of fixing the Case of a property or commenting-out any duplicates.

Once the inevitable inconsitencies in the schemas are resolved, you should be able to import the pom changes
into idea so that the regenerated classes are picked up and you should be good to go :D

