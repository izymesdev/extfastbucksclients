package com.izymes.jira.fastbucks.clients.xero;


import com.google.common.collect.Maps;
import com.izymes.jira.fastbucks.clients.xero.generated.ResponseType;
import net.oauth.*;
import net.oauth.client.httpclient3.HttpClient3;
import net.oauth.signature.RSA_SHA1;

import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.util.Map;

public class XeroConnectionImpl implements XeroConnection {
    private  Map<String, String> configMap = Maps.newHashMap();
    private String uri;

    public XeroConnectionImpl(Map<String, String> xeroParams) {
        this.configMap = xeroParams;
    }

    @Override
    public XeroConnection setUri(String uri){
        this.uri = uri;
        return this;
    }

    @Override
    public ResponseType post(String path, Map<String, String> params) throws IOException, URISyntaxException, OAuthException {
        OAuthAccessor oauthAccessor = buildAccessor();
        net.oauth.client.OAuthClient client = new net.oauth.client.OAuthClient(new HttpClient3());
        OAuthMessage message = null;

        ResponseType  response = null;
        message = client.invoke(oauthAccessor, OAuthMessage.POST, path , params.entrySet() );
        response = getResponse(message.getBodyAsStream());

        return response;

    }

    @Override
    public ResponseType get( String path ) throws IOException, URISyntaxException, OAuthException {
        OAuthAccessor oauthAccessor = buildAccessor();
        net.oauth.client.OAuthClient client = new net.oauth.client.OAuthClient(new HttpClient3());
        OAuthMessage message = null;

        ResponseType  response = null;
        message = client.invoke(oauthAccessor, OAuthMessage.GET, path, null);
        response = getResponse(message.getBodyAsStream());

        return response;

    }
    @Deprecated
    @Override
    public ResponseType get() throws IOException, URISyntaxException, OAuthException {
        return get( uri );
    }

    private OAuthAccessor buildAccessor() {

        String consumerKey = configMap.get("com.izymes.jira.fastbucks.client.xero.consumerkey");
//        String privateKey =  this.privateKey;
        String privateKey =  configMap.get("com.izymes.jira.fastbucks.client.xero.privatekey");
        String consumerSecret = configMap.get("com.izymes.jira.fastbucks.client.xero.consumersecret");

        OAuthConsumer consumer = new OAuthConsumer(null, consumerKey, null, null);
        consumer.setProperty(RSA_SHA1.PRIVATE_KEY, privateKey);
        consumer.setProperty(OAuth.OAUTH_SIGNATURE_METHOD, OAuth.RSA_SHA1);

        OAuthAccessor accessor = new OAuthAccessor(consumer);
        accessor.accessToken = consumerKey;
        accessor.tokenSecret = consumerSecret;

        return accessor;
    }

    private ResponseType getResponse(InputStream inputStream)
    {
        ResponseType response = null;
            response = XmlUtils.getInstance().getEntity(inputStream, ResponseType.class);
        return response;
    }


}
