package com.izymes.jira.fastbucks.clients.xero;


import javax.xml.bind.*;
import javax.xml.namespace.QName;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;

public class XmlUtils
{
    private JAXBContext jaxbContext = null;
    private static XmlUtils INSTANCE = new XmlUtils();
    private XmlUtils()
    {
        try
        {
//            jaxbContext = javax.xml.bind.JAXBContext.newInstance("com.izymes.xero.generated", JAXBContext.class.getClassLoader());
            jaxbContext = JAXBContext.newInstance("com.izymes.jira.fastbucks.clients.xero.generated", this.getClass().getClassLoader());
        } catch (JAXBException e)
        {
            e.printStackTrace();
        }
    }

    public static XmlUtils getInstance()
    {return INSTANCE;}

    public JAXBContext getJaxbContext() {
        if (jaxbContext == null ) {
            try {
                jaxbContext = JAXBContext.newInstance("com.izymes.jira.fastbucks.clients.xero.generated", this.getClass().getClassLoader());
            } catch (JAXBException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }
        return jaxbContext;
    }

    public void setJaxbContext(JAXBContext jaxbContext) {
        this.jaxbContext = jaxbContext;
    }

    public ByteArrayOutputStream generateXml(Object data, String qName) throws JAXBException

    {
        // generate xml from object
        ByteArrayOutputStream os = new ByteArrayOutputStream();
            Marshaller marshaller = getJaxbContext().createMarshaller();
            JAXBElement element = new JAXBElement(new QName("", qName), data.getClass(), data);
            marshaller.marshal(element, os);
            return os;
    }


    public <T> T getEntity(InputStream is, Class<T> c)
    {
        try
        {
            Unmarshaller u = getJaxbContext().createUnmarshaller();
            JAXBElement<T> element = (JAXBElement) u.unmarshal(is);
            return element.getValue();

        } catch (JAXBException e)
        {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        return null;
    }

}
